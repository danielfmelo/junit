package model;

import java.util.ArrayList;
import java.util.Arrays;

public class Student {
    private String name;
    private String idNumber;
    private String courses[];

    public String getName() {
        return name;
    }

    public String[] getCourses() {
        return courses;
    }

    public void setCourses(String[] courses) {
        this.courses = courses;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }


    public Student(String name, String idNumber, String[] courses) {

        this.name = name;
        this.idNumber = idNumber;
        this.courses = courses;

        courses[0] = "CIT360";
        courses[1] = "CIT325";
        courses[2] = "FDSCI101";
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", idNumber='" + idNumber + '\'' +
                ", courses=" + Arrays.toString(courses) +
                '}';
    }
}
