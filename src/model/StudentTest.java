package model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class StudentTest {

    String courses[] = new String[3];
    Student student = new Student("Daniel", "2350205", courses);

    @Test
    void testName() {
        assertEquals(student.getName(), "Daniel", "The name is not equals to " + student.getName());
    }

    @Test
    void testIfNotNull() {
        assertNotNull(student, "The student object is null");
    }

    @Test
    void testIfNull() {
        Student student1 = null;
        assertNull(student1, "The student object isn\'t null");
    }

    @Test
    void testGetCourses() {
        String courses1[] = new String[3];
        courses1[0] = "CIT360";
        courses1[1] = "CIT325";
        courses1[2] = "FDSCI101";

        assertArrayEquals(student.getCourses(), courses1, "The arrays are not equal");

    }

    @Test
    void testSameIdNumber() {
        assertSame(student.getIdNumber(), "23502059", "The ID Number doesnt match");
    }

    @Test
    void testNotSameIdNumber() {
        assertNotSame(student.getIdNumber(), "2350205", "The ID Numbers match");
    }

}